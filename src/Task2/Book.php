<?php

declare(strict_types=1);

namespace App\Task2;

use Exception;

class Book
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var int
     */
    private int $price;

    /**
     * @var int
     */
    private int $pagesNumber;

    public function __construct(string $title, int $price, int $pagesNumber)
    {
        $this->title = $title;

        if ($price <= 0) {
            throw new Exception('Negative price');
        }
        $this->price = $price;

        if ($pagesNumber <= 0) {
            throw new Exception('Negative number of pages');
        }
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}