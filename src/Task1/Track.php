<?php

declare(strict_types=1);

namespace App\Task1;

use Exception;

class Track
{
    /**
     * @var float
     */
    private float $lapLength;

    /**
     * @var int
     */
    private int $lapsNumber;

    /**
     * @var array
     */
    private array $cars;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ($lapLength <= 0) {
            throw new Exception('Negative lap length');
        }
        $this->lapLength = $lapLength;

        if ($lapLength <= 0) {
            throw new Exception('Negative laps number');
        }
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        if ($this->cars === null) {
            throw new Exception('No cars');
        }

        return $this->cars;
    }

    public function run(): Car
    {
        $trackLenght = $this->getLapLength() * $this->getLapsNumber();

        $winner = null;
        $minTime = null;
        foreach ($this->all() as $car) {
            $timeForTrackInSeconds = $trackLenght / $car->getSpeed() * 3600;
            $pitStopTimeInSeconds = floor(($trackLenght / 100 * $car->getFuelConsumption())
                    / $car->getFuelTankVolume() ) * $car->getPitStopTime();

            $totalTime = $timeForTrackInSeconds + $pitStopTimeInSeconds;

            if ($minTime === null || $minTime > $totalTime) {
                $minTime = $totalTime;
                $winner = $car;
            }
        }

        return $winner;
    }
}