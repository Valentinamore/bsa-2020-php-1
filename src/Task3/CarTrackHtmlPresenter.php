<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        return '<img src="https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg">BMW: 1, 2
                <img src="https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg">Tesla: 1, 2
                <img src="https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg">Ford: 1, 2';
    }
}